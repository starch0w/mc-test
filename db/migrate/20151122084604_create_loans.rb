class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.string :address
      t.integer :property_type
      t.decimal :mortgage_payment
      t.decimal :mortgage_insurance
      t.decimal :homeowner_insurance
      t.decimal :property_tax
      t.decimal :hoa_due
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :credit_score
      t.decimal :base_income
      t.decimal :rental_income
      t.decimal :commission

      t.timestamps null: false
    end
  end
end
