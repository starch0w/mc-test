json.array!(@loans) do |loan|
  json.extract! loan, :id, :address, :property_type, :mortgage_payment, :mortgage_insurance, :homeowner_insurance, :property_tax, :hoa_due, :first_name, :last_name, :email, :credit_score, :base_income, :rental_income, :commission
  json.url loan_url(loan, format: :json)
end
