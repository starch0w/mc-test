class Loan < ActiveRecord::Base
  enum property_type: ['single_family', 'duplex', 'triplex', '4-plex', 'condo']

  def total_income
    (base_income + rental_income + commission).to_f.round(2)
  end

  def housing_expense_ratio
    ((mortgage_payment + mortgage_insurance + homeowner_insurance + property_tax + hoa_due) / total_income).round(2)
  end
end
