class CheckLoanService
  def initialize(loan)
    @loan = loan
    @error_message = nil
  end

  def is_valid?
    return false unless check_property_type
    return false unless check_borrower_credit_score
    return false unless check_housing_expense_ratio
    true
  end

  def error_message
    @error_message
  end

  private
  def check_property_type
    types = ['single_family', 'duplex', 'triplex', '4-plex', 'condo']
    @error_message = I18n.t('errors.property_type') unless types.include?(@loan.property_type)
    @error_message.blank?
  end

  def check_borrower_credit_score
    @error_message = I18n.t('errors.borrower_credit_score') if @loan.credit_score.to_i < 620
    @error_message.blank?
  end

  def check_housing_expense_ratio
    @error_message = I18n.t('errors.housing_expense_ratio') if @loan.housing_expense_ratio > 0.28
    @error_message.blank?
  end
end
