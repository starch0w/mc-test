require 'rails_helper'

RSpec.describe CheckLoanService, type: :model do
  it "property type must valid" do
    loan = Loan.new()
    service_object = CheckLoanService.new(loan)

    service_object.is_valid?
    expect(service_object.is_valid?).to eq(false)
    expect(service_object.error_message).to eq(I18n.t('errors.property_type'))
  end

  it "borrower credit score must valid" do
    random_type = Loan.property_types.keys.sample
    credit_score = Faker::Number.between(0, 620).to_i
    loan = Loan.new(property_type: random_type, credit_score: credit_score)
    service_object = CheckLoanService.new(loan)

    service_object.is_valid?
    expect(service_object.is_valid?).to eq(false)
    expect(service_object.error_message).to eq(I18n.t('errors.borrower_credit_score'))
  end

  it "housing expense ratio must valid" do
    random_type = Loan.property_types.keys.sample
    credit_score = Faker::Number.between(621, 9999).to_i
    base_income = Faker::Number.decimal(5).to_f
    rental_income = Faker::Number.decimal(4).to_f
    commission = Faker::Number.decimal(4).to_f
    mortgage_payment = base_income
    mortgage_insurance = rental_income
    homeowner_insurance = commission
    property_tax = base_income
    hoa_due = base_income

    loan = Loan.new(base_income: base_income, rental_income: rental_income, commission: commission, mortgage_payment: mortgage_payment, mortgage_insurance: mortgage_insurance, homeowner_insurance: homeowner_insurance, property_tax: property_tax, hoa_due: hoa_due, property_type: random_type, credit_score: credit_score)
    service_object = CheckLoanService.new(loan)

    service_object.is_valid?
    expect(service_object.is_valid?).to eq(false)
    expect(service_object.error_message).to eq(I18n.t('errors.housing_expense_ratio'))
  end

  it "loan is valid" do
    random_type = Loan.property_types.keys.sample
    credit_score = Faker::Number.between(621, 9999).to_i
    base_income = Faker::Number.decimal(6).to_f
    rental_income = Faker::Number.decimal(4).to_f
    commission = Faker::Number.decimal(4).to_f
    mortgage_payment = rental_income
    mortgage_insurance = rental_income
    homeowner_insurance = commission
    property_tax = 0
    hoa_due = 0

    loan = Loan.new(base_income: base_income, rental_income: rental_income, commission: commission, mortgage_payment: mortgage_payment, mortgage_insurance: mortgage_insurance, homeowner_insurance: homeowner_insurance, property_tax: property_tax, hoa_due: hoa_due, property_type: random_type, credit_score: credit_score)
    service_object = CheckLoanService.new(loan)

    service_object.is_valid?
    expect(service_object.is_valid?).to eq(true)
    expect(service_object.error_message).to eq(nil)
  end
end
