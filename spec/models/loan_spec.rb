require 'rails_helper'

RSpec.describe Loan, type: :model do
  it "total income must be total from base income, rental income and comission" do
    base_income = Faker::Number.decimal(5).to_f
    rental_income = Faker::Number.decimal(4).to_f
    commission = Faker::Number.decimal(4).to_f
    total_income = (base_income + rental_income + commission).round(2)
    loan = Loan.new(base_income: base_income, rental_income: rental_income, commission: commission)

    expect(loan.total_income).to eq(total_income)
  end

  it "housing expense ratio" do
    base_income = Faker::Number.decimal(5).to_f
    rental_income = Faker::Number.decimal(4).to_f
    commission = Faker::Number.decimal(4).to_f
    mortgage_payment = Faker::Number.decimal(6).to_f
    mortgage_insurance = Faker::Number.decimal(4).to_f
    homeowner_insurance = Faker::Number.decimal(4).to_f
    property_tax = Faker::Number.decimal(4).to_f
    hoa_due = Faker::Number.decimal(4).to_f
    total_income = (base_income + rental_income + commission).round(2)
    housing_expense_ratio = ((mortgage_payment + mortgage_insurance + homeowner_insurance + property_tax + hoa_due) / total_income).round(2)

    loan = Loan.new(base_income: base_income, rental_income: rental_income, commission: commission, mortgage_payment: mortgage_payment, mortgage_insurance: mortgage_insurance, homeowner_insurance: homeowner_insurance, property_tax: property_tax, hoa_due: hoa_due)

    expect(loan.housing_expense_ratio).to eq(housing_expense_ratio)
  end
end
